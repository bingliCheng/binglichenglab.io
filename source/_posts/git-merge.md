---
title: Git merge
date: 2019-08-14 12:18:00
tags: Git
category: Git
---
### 将gitlab中的beta分支里的数据备份于master分支内。
```
$ git branch # 先查看自己当前branch
* beta
  master
$ git checkout master # 切换到master分支
M	_config.yml
M	db.json
M	themes/life/source/css/atom-one-dark.css
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
$ git merge beta  # 备份beta分支内的数据
$ git push --set-upstream master  # push
$ git checkout beta # 切换回beta分支
```

---
参考文章：
[创建与合并分支-廖雪峰](https://www.liaoxuefeng.com/wiki/896043488029600/900003767775424)
