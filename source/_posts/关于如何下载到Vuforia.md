---
title: 关于如何下载到Vuforia
date: 2019-06-26 13:50
tags: Unity
category: Unity
---
在学unity这块AR，就上到高通AR也就是[Vuforia官网](https://developer.vuforia.com)，我真的也是醉了，最新版本你给出来了个Mac电脑可以如何下载，但是我是Windows系统啊，难道你官网还歧视Windows系统吗？想下载都没地方下载。
最终我无语了，付费了个9.9买了个蛮牛会员，因为我知道有一节【会员免费】课里有提供Vuforia下载，导入Unity发现还是不是我想要的那个...国内搜索引擎也搜索不到，我就只好用英文去搜，最后在github上找到了。

[Unity](https://answers.unity.com/questions/1462338/where-can-i-download-old-version-of-vuforia-sample.html)社区
[Github](https://github.com/maximrouf/All_Vuforia_versions)地址
以及我把这些上传到百度云的地址[百度云](https://pan.baidu.com/s/1GQsEkZwabpm9h7jXCxbscQ) 提取码：11qt

我是真的无语，下载都不行最后还是上淘宝买csdn积分买了一个。[百度云地址在这](https://pan.baidu.com/s/1AqKx-Phr2FGMkuNNs6tS-A) 提取码：a249 
